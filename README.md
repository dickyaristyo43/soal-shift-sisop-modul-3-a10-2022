Praktikum Sistem Operasi (A)
- Alif Adrian Anzary - 5025201274
- Ferdinand Putra Gumilang Silalahi - 5025201176
- Elbert Dicky Aristyo - 5025201231

Nomor 1

Revisi Soal

Dalam penggunaan multi-threading di satu proses, akan dibutuhkan fungsi untuk mewakili satu demi satu thread yang dijalankan. Beberapa thread, seperti soal 1.a mengharuskan untuk dijalankan dalam waktu yang bersamaan. Hal ini akan membedakan penggunaan fungsi pthread_join(), yakni fungsi wait yang ada pada thread.

1.a

Kita akan menggunakan 2 thread untuk menyelesaikan sub-soal ini. Thread pertama digunakan untuk unzip music.txt dan thread kedua digunakan untuk unzip quote.txt. Kedua thread ini akan di create bersamaan baru di-joinkan pada main-process agar proses kedua thread berjalan bersamaan. 

1.b,c

Sebagai persiapan sebelum men-decode base64. Kita akan membuat quote.txt dan music.txt yang nantinya akan menjadi list dari base64 yang telah di-decode. Kemudian, kita akan melakukan listing direktori yang berisi file-file yang berasal dari hasil extract quote.zip dan music.zip. Setelah itu kita baru listing untuk men-decodenya satu per satu.
Pertama-tama kita harus membuat fungsi utilitas yang nantinya akan digunakan untuk men-decode base64.
Untuk utilitas pertama adalah decode table yang digunakan untuk mendecode base64 ke binary.
Untuk utilitas kedua adalah decode size yang berfungsi untuk menentukan ukuran dari binary yang menjadi hasil decode dari base64.
Utilitas ketiga adalah untuk menentukan apakah base64 memiliki karakter yang valid atau tidak.
Kemudian, kita akan membuat fungsi utama untuk mendecode b64 menjadi binary. Fungsi ini akan membutuhkan beberapa parameter input, yakni: 1. Pointer dari string yang masih dalam format base64, 2. Pointer dari string yang akan menjadi hasil decode, dan 3. Panjang/ukuran dari string hasil decode yang didapat dari utiliitas kedua.
Setelah semua .txt sudah di decode dan dimasukkan pada .txt tujuan yang sesuai, kita akan close semua stream yang digunakan.

1.d

Untuk melakukan zip pada sebuah folder, kita dapat menggunakan fungsi exec() dan menggunakan fungsi bash zip. Untuk memberikan password pada zip tersebut, kita dapat menggunakan -P diikuti dengan password yang kita inginkan (pada kasus ini passwordnya: mihinomenestandrian).

1.e

Pertama-tama kita harus membuat no.txt dan menuliskan konten "No". Setelah itu, kita akan meng-zipnya bersamaan dengan folder hasil dengan menambahkan -r karena hasil merupakan folder, sedangkan no.txt merupakan file.

Kendala Soal

Kadang bisa terjadi error saat menjalankan program.

Nomor 2

Revisi Soal

Karena nomor 2 membutuhkan memiliki 2 program yang saling terkoneksi satu sama lain, maka kita akan menggunakan Socket Programming untuk membuat koneksi 2 program tersebut. Untuk pengaturan koneksi awal, kita dapat mencocokkan port dan IP, sehingga server dan client dapat terkoneksi.

2.a 

Karena server dan client sudah terhubung, kita diminta untuk memberikan beberapa command yang dapat dicantumkan oleh client pada saat masih dalam posisi LOGOUT_STATE, yakni register dan login.
Untuk register, kita harus memasukkan data username dan password client yang kemudian diterima oleh server dan disimpan pada database. Akan tetapi, ada beberapa syarat. Username harus bersifat unik dan password minimal terdiri dari 6 huruf, terdapat angka, terdapat huruf besar dan kecil. Setelah data akun baru diterima oleh server. Data tersebut akan dituliskan pada user.txt. 

2.b

Server diminta untuk membuat problems.tsv jika belum tersedia sebelumnya. Server akan menggunakan fopen untuk membuat file tersebut.

2.c

Client yang telah login diharapkan untuk mendapat 4 command. Command yang pertama adalah "add" dimana client dapat memasukkan problem baru pada database server. Problem baru akan disimpan pada problems.tsv dengan format 'judul-problems/tauthor'. Setelah itu, akan dibuat folder dengan nama judul problem baru yang berisi deskripsi, input, dan output dari problem baru tersebut.

2.d

Command kedua adalah see, dimana client dapat melihat list dari problems yang tersedia di database problems yang berada server. Server dapat menampilkan apa yang tertulis di problems.tsv dan mengembalikan informasinya dalam format yang tepat pada client.

2.e 

Client dapat melakukan download soal dengan judul tertentu dengan memasukkan command "download 'judul-problem'". Dengan memasukkan command tersebut, server akan mengirimkan informasi deskripsi dan input dari problem tersebut. Selain itu, server akan membuat folder tempat menyimpan deskripsi dari input problem tersebut pada tempat penyimpanan client.

2.f 

Client dapat melakukan submit pada problem dengan judul tertentu yang kemudian akan di cek oleh server apakah jawaban yang diberikan benar atau salah. Format command submit dari client adalah "submit 'judul-problem' 'filepath output dari client'".

2.g 

Server dapat menampung lebih dari 1 koneksi client dengan menggunakan thread. Thread/client lain akan menunggu hingga thread pertama sudah selesai diproses oleh server.

Nomor 3

Revisi Soal

Program dijalankan untuk mengategorikan file-file secara rekursif berdasarkan jenis ekstensinya dengan cara memindahkan file-file tersebut ke dalam directori bernama jenis ekstensinya masing masing. Perlu diketahui bahwa nama direktori ekstensinya semuanya lowercase, agar dapat mengakomodasi ekstensi yang tidak case sensitive. Mengategorikan semua file di dalam working directory semua directori ekstensi terletak di directory yang sama dengan file yang dijalankan.

Jika file tidak memiliki ekstensi, maka filenya diletakan di direktori "Unknown". Jika file dalam mode hidden, maka filenya diletakan di direktori "Hidden". Setiap file yang dipindahkan memiliki satu thread agar prosesnya bisa berjalan secara bersamaan.

Kemudian program dijalankan dengan menggunakan client server dan pada saat client dijalankan maka directori hartakarun akan di zip dengan nama hartakarun.zip ke working directori client dan client dapat mengirimkan file hartakarun.zip tersebut ke server dengan mengirimkan command ke server "send hartakarun.zip"

3.a, b

Terdapat dua fungsi pada file soal3.c ini yaitu handler dan listFilesRecursively. Untuk working directorinya dibuat dengan menggunakan program yaitu dengan menggunakan fork() seperti pada modul sebelumnya. Kemudian berikut merupakan penjelasan dari fungsi fungsi yang ada pada file soal3.c
~~~
void* handler(void *arg)
{
    
    char things[200];
    strcpy(things,arg);

	unsigned long i=0;
	pthread_t id=pthread_self();
	int iter, index=0;
    char *arr2[50];
    char namaFile[200];
    char argCopy[100];

    //ngambil namafile beserta eksistensi
    char *token1 = strtok(things, "/");
    while (token1 != NULL) {
            arr2[index++] = token1;
            token1 = strtok(NULL, "/");
    }
    strcpy(namaFile,arr2[index-1]);

    //cek filenya termasuk dalam folder apa
    char *token = strchr(namaFile, '.');
    if (token == NULL) {
        strcat(argCopy, "Unknown");
    }
    else if (namaFile[0] == '.') {
        strcat(argCopy, "Hidden");
    }
    else {
        strcpy(argCopy, token+1);
        for (int i = 0; argCopy[i]; i++) {
            argCopy[i] = tolower(argCopy[i]);
        }
    }

    char source[1000], target[1000], dirToBeCreate[120];
    char *unhide, unhidden[200];
    strcpy(source, arg);
    if (sinyal == 1) {
        sprintf(target, "/home/andrianalif/shift3/hartakarun/%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "/home/andrianalif/shift3/hartakarun/%s/", argCopy);
        mkdir(dirToBeCreate, 0750);   
    } else if (sinyal == 2 || sinyal == 3) {
        if (namaFile[0] == '.') {
            namaFile[0] = '-';
        }
        sprintf(target, "%s/%s", argCopy, namaFile);
        sprintf(dirToBeCreate, "%s/", argCopy);
        mkdir(dirToBeCreate, 0750);
    }

    //pindah file
    if (rename(source,target) == 0) {
        printf("Berhasil Dikategorikan\n");
    } else printf("Gagal dikategorikan :(\n");

    return NULL;
}
~~~

- Fungsi tersebut akan mengecek apakah file tersebut memiliki ekstensi, tidak memiliki ekstensi, atau hidden (Yang berawalan titik). Lalu file tersebut dipindahkan ke folder yang sesuai dengan ekstensinya.
- Setelah mengkategorikan folder tujuan, directory ditentukan berdasarkan sinyal. Pada variable sinyal, bila bernilai 1 maka directory tujuan ditentukan pada "/home/[user]/modul3/hartakarun/". Bila sinyal bernilai 2 atau 3 maka directory tujuan akan diset dimana program C dijalankan.

Kemudian untuk menyusun daftar file file dalam directori terdapat fungsi listFilesRecursively yang akan menyusun file file tersebut sesuai dengan ekstensinya secara rekursif.
~~~
void listFilesRecursively(char *basePath){
    char path[256]={};
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
    return;

    while ((dp = readdir(dir)) != NULL)
    {
        if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0)
        {
            if (dp->d_type == DT_REG)
            {
                strcpy(tanda[x], basePath);
                strcat(tanda[x], dp->d_name);
                x++;
            }
            else
            {
                strcpy(path, basePath);
                strcat(path, dp->d_name);
                strcat(path, "/");
                listFilesRecursively(path);
            }
        }
    }
    closedir(dir);
}
~~~

- File-file di dalam direktori dan juga file-file di dalam subdirektori, semuanya disusun dalam sekumpulan array string yang dapat dikategorikan dengan fungsi persoalan pertama tadi. Untuk mengkategorikan file-file di dalam subdirektori, digunakan rekursi fungsi untuk menata daftar file di dalam direktori tersebut.

3.c

Agar proses kategori bisa berjalan lebih cepat, setiap 1 file yang dikategorikan dioperasikan oleh 1 thread. Pada fungsi main() terdapat program sebagai berikut : 
~~~
sinyal = 3;
    int err;
    listFilesRecursively(path);

    for (i=0; i<x; i++){
	    err=pthread_create(&(thd[i]),NULL,&handler,(void *) tanda[i]);
	    
        if(err!=0) return 0;
    }
    for (i=0; i<x; i++) pthread_join(thd[i],NULL);
~~~

3.d, e

Dengan menggunakan program client server, client dapat mengirimkan zip hasil pemrosesan yang dilakukan pada program soal.c ini ke dalam directori server dengan mengirimkan command send hartakarun.zip
~~~
void* zip_file() {
    pid_t child_id;
    int status1;
        
    child_id = fork();
    if (child_id == 0) {
        printf("haloo");
        char *argv[] = {"zip", "-q", "-r", "hartakarun.zip", "/home/andrianalif/shift3/hartakarun", NULL};
        execv("/usr/bin/zip", argv);
    } else {
        while (wait(&status1) > 0);
    }
}
~~~

- Pada bagian client ini pertama akan melakukan zip pada directori "/home/[user]/shift3/hartakarun/" dengan memangggil fungsi zip_file() pada fungsi main kemudian pada program client ini dapat mengirimkan command yang nantinya zip file tersebut akan kekirim kedalam direcotri servernya.
